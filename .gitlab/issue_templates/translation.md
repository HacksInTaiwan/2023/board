> ✳️ 請先閱讀[翻譯組工作流程](https://gitlab.com/HacksInTaiwan/2023/board/-/wikis/translation-workflow)，並將本行刪除。

### 需求描述

**性質**：（網站文案／書信／社群媒體文案／⋯⋯）

**用途描述**：（受眾對象？）

**預計在何時使用**：（可善用 `/due`）


### 翻譯內容

（將文案內容或是連結貼在這裡）

### 參考資料

1. （相關補充資料、活動描述、圖文連結、附件）

2. 

---

/label ~"Team::翻譯" ~"Status::Inbox" ~"文案翻譯"
/assign @RSChiang
